- vagrant up
- sshクライアントでログイン
-- IPアドレス:192.168.33.100
-- ポート:22
-- ユーザー:vagrant
-- パスワード:vagrant
- cd /vagrant/setup
- ./main.sh
- cd /share
- mkdir my-new-project && cd $_
- yo angular-fullstack
- Gruntfile.jsを編集
 - 以下の記述を削除
  - ``
open: {
  server: {
    url: 'http://localhost:<%= express.options.port %>'
  }
},
``
  - ``
require('open')('http://localhost:8080/debug?port=5858');
``
 - 以下の記述を編集
  - 編集前
``
return grunt.task.run(['build', 'env:all', 'env:prod', 'express:prod', 'wait', 'open', 'express-keepalive']);
``
編集後「'open'」を削除
``
return grunt.task.run(['build', 'env:all', 'env:prod', 'express:prod', 'wait', 'express-keepalive']);
``
  - 編集前
``
    grunt.task.run([
      'clean:server',
      'env:all',
      'injector:sass', 
      'concurrent:server',
      'injector',
      'wiredep',
      'autoprefixer',
      'express:dev',
      'wait',
      'open',
      'watch'
    ]);
``
編集後「'open'」を削除
``
    grunt.task.run([
      'clean:server',
      'env:all',
      'injector:sass', 
      'concurrent:server',
      'injector',
      'wiredep',
      'autoprefixer',
      'express:dev',
      'wait',
      'watch'
    ]);
``
- grunt serve
- ブラウザで「192.168.33.10:9000」にアクセス

# \\\\192.168.33.10\shareでゲストOSの「/share」ディレクトリにアクセス可能
