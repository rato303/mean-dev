#!/bin/sh

EXECUTE_PATH=`pwd`

echo "##### インストール変数を読み込みます。 #####" ; source ${EXECUTE_PATH}/env.sh

echo "##### 開発環境構築に必要なパッケージをインストールします。 #####" ; . ${EXECUTE_PATH}/installer/install_package.sh

echo "##### sambaインストーラーを開始します。 #####" ; . ${EXECUTE_PATH}/installer/install_samba.sh

echo "##### rbenvインストーラーを開始します。 #####" ; . ${EXECUTE_PATH}/installer/install_rbenv.sh

echo "##### mean環境構築インストーラーを開始します。 #####" ; . ${EXECUTE_PATH}/installer/install_mean.sh

echo "##### nginxインストーラーを開始します。 #####" ; . ${EXECUTE_PATH}/installer/install_nginx.sh
