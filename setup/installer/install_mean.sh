#!/bin/sh

echo "##### nvm(${NVM_VERSION})をインストールします。 #####" ; curl -o- https://raw.githubusercontent.com/creationix/nvm/${NVM_VERSION}/install.sh | bash

echo "##### nvmを有効化します。 #####" ; . ${HOME}/.nvm/nvm.sh

echo "##### node.js(${NODE_JS_VERSION})をインストールします。 #####" ; nvm install ${NODE_JS_VERSION}

echo "##### yeoman,grunt,bower,generator-angular-fullstackをインストールします。 #####" ; npm install -g yo grunt-cli bower generator-angular-fullstack

echo "##### シェルログイン時にnvmを有効化するように設定します。 #####"
echo "source ${HOME}/.nvm/nvm.sh" >> ${HOME}/.bash_profile
echo "nvm use ${NODE_JS_VERSION}" >> ${HOME}/.bash_profile

echo "##### mongodbのyumリポジトリを追加します。 #####"
sudo sh -c "echo '[mongodb-org-3.0]' >> /etc/yum.repos.d/mongodb-org-3.0.repo"
sudo sh -c "echo 'name=MongoDB Repository' >> /etc/yum.repos.d/mongodb-org-3.0.repo"
sudo sh -c "echo 'baseurl=http://repo.mongodb.org/yum/redhat/7Server/mongodb-org/3.0/x86_64/' >> /etc/yum.repos.d/mongodb-org-3.0.repo"
sudo sh -c "echo 'gpgcheck=0' >> /etc/yum.repos.d/mongodb-org-3.0.repo"
sudo sh -c "echo 'enabled=1' >> /etc/yum.repos.d/mongodb-org-3.0.repo"

echo "##### mongodbをインストールします。 #####" ; sudo yum install -y mongodb-org

echo "##### mongodサービスを起動します。 #####" ; sudo service mongod start

echo "##### mongodサービスの自動起動設定をします。 #####" ; sudo chkconfig mongod on
