#!/bin/sh

echo "##### sambaをインストールします。 #####" ; sudo yum install -y samba

echo "##### sambaで共有するディレクトリ(/share)を作成します。 #####" ; sudo mkdir /share

echo "##### sambaで共有するディレクトリの所有者をvagrantに変更します。 #####" ; sudo chown vagrant:vagrant /share

echo "##### sambaの設定ファイルをVagrant共有ディレクトリからコピーします。 #####" ; sudo cp /vagrant/setup/resources/samba/smb.conf /etc/samba/

echo "##### sambaの設定ファイルの所有者をrootに変更します。 #####" ; sudo chown root:root /etc/samba/smb.conf

echo "##### sambaの設定ファイルの権限を変更(644)します。 #####" ; sudo chmod 644 /etc/samba/smb.conf

echo "##### smbサービスを起動します。 #####" ; sudo systemctl start smb

echo "##### nmbサービスを起動します。 #####" ; sudo systemctl start nmb

echo "##### smbサービスの自動起動設定をします。 #####" ; sudo systemctl enable smb

echo "##### nmbサービスの自動起動設定をします。 #####" ; sudo systemctl enable nmb
