#!/bin/sh

echo "##### rbenvをgit cloneします。 #####" ; git clone https://github.com/sstephenson/rbenv.git ~/.rbenv

echo "##### rbenvのパスを${HOME}/.bash_profileに追加します。 #####"
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ${HOME}/.bash_profile
echo 'eval "$(rbenv init -)"' >> ${HOME}/.bash_profile

echo "##### ${HOME}/.bash_profile を再読み込みします。 #####" ; source ${HOME}/.bash_profile

echo "##### ruby-buildをgit cloneします。 #####" ; git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

echo "##### ruby(${RUBY_VERSION})をインストールします。 #####" ; rbenv install ${RUBY_VERSION}

echo "##### ruby($RUBY_VERSION)を有効化します。 #####" ; rbenv global ${RUBY_VERSION}

echo "##### gemを更新します。 #####" ; gem update --system

echo "##### bundlerをインストールします。 #####" ; gem install bundler

echo "##### compassをインストールします。 #####" ; gem install compass
