#!/bin/sh

echo "##### nginxのyumリポジトリを追加します。 #####"
sudo sh -c "echo '[nginx]' >> /etc/yum.repos.d/nginx.repo"
sudo sh -c "echo 'name=nginx repo' >> /etc/yum.repos.d/nginx.repo"
sudo sh -c "echo 'baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/' >> /etc/yum.repos.d/nginx.repo"
sudo sh -c "echo 'gpgcheck=0' >> /etc/yum.repos.d/nginx.repo"
sudo sh -c "echo 'enabled=1' >> /etc/yum.repos.d/nginx.repo"

echo "##### nginxをインストールします。 #####" ; sudo yum install -y nginx
