#!/bin/sh

# インストールするrubyのバージョン
RUBY_VERSION=2.2.2

# インストールするnvmのバージョン
NVM_VERSION=v0.25.4

# インストールするnode.jsのバージョン
NODE_JS_VERSION=v0.12.4
